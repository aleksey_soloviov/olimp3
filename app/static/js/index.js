$(document).ready(function() {
    $('#inventory').on('shown.bs.modal', function () {
        var inventoryForm = $('#inventory-form .modal-body');
        $.ajax({
            type: "GET",
            url: "/inventory",
            success: function (data) {
                console.log(data)  // display the returned data in the console.

                inventoryForm.empty();
                inventoryForm.append(data.inventory);
//                inventoryForm.append('<table class="table table-borderles"> \
//                                         <thead> \
//                                            <tr> \
//                                                <th>#</th> \
//                                                <th>Код</th> \
//                                                <th>Наименование</th> \
//                                                <th>Кол-во БД</th> \
//                                                <th>Кол-во факт</th> \
//                                            </tr> \
//                                         </thead> \
//                                         <tbody> \
//                                         </tbody> \
//                                       </table>');
//                var invTableBody = $('#inventory-form .modal-body tbody');
//                data.inventory.forEach(function(d){
//                    var inventoryRow = $('<tr></tr>');
//                    inventoryRow.append('<td><input name="row_number[]" type="number" class="text-right border-0 num-input" readonly value="' + d.row_number + '"></td>');
//                    inventoryRow.append('<td><input name="good_code[]" type="text" size="6" class="border-0" readonly value="' + d.good_code + '"></td>');
//                    inventoryRow.append('<td><input name="good_name[]" type="text" size="50" class="border-0" readonly value="' + d.good_name + '"></td>');
//                    inventoryRow.append('<td><input name="quantity_db[]" type="number" class="text-right num-input border-0" readonly value="' + d.quantity_db + '"></td>');
//                    inventoryRow.append('<td><input name="quantity_fact[]" type="number" class="text-right num-input" value="' + d.quantity_fact + '"></td>');
//                    invTableBody.append(inventoryRow);
//                });
            },
            error: function(data) {
                inventoryForm.empty();
                inventoryForm.append('<div class="alert alert-danger" role="alert"> \
                                          Нет соединения с сервером! \
                                        </div>');
                // $('#inventory-submit').;
            }
        });
    });

});

function inventorySubmit() {
    $('#inventory-form').submit(function (e) {
        var url = "/inventory";  // send the form data here.
        var form = $(this);
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data) {
                // alert(data)  // display the returned data in the console.
                $('#inventory').modal('hide');
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            }
        });
        e.preventDefault(); // block the traditional submission of the form.
    });
}