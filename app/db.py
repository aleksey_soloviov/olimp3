# coding: utf-8
from pprint import pprint


def get_goods_balance():
    """Функция получает остатки товаров из базы данных"""
    data = [
        {'row_number': 1, 'good_id': 0, 'good_code': '00001', 'good_name': 'Товар 1', 'quantity_db': 100},
        {'row_number': 2, 'good_id': 1, 'good_code': '00002', 'good_name': 'Товар 2', 'quantity_db': 45},
        {'row_number': 3, 'good_id': 2, 'good_code': '00003', 'good_name': 'Товар 3', 'quantity_db': 131},
        {'row_number': 4, 'good_id': 3, 'good_code': '00004', 'good_name': 'Товар 4', 'quantity_db': 11},
        {'row_number': 5, 'good_id': 4, 'good_code': '00005', 'good_name': 'Товар 5', 'quantity_db': 7},
    ]
    # заполняем фактическое кол-во по-умолчанию
    for item in data:
        item['quantity_fact'] = item['quantity_db']
    return data


def set_goods_balance(data):
    """Функция записывает фактические остатки товаров в базу данных"""
    pprint(data)  # пока так
