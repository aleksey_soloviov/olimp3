# coding: utf-8
from flask import render_template, jsonify, request
from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, FieldList, FormField
from wtforms.validators import NumberRange
from jinja2 import Template
from app import app
from . import db


@app.route('/')
@app.route('/index')
def index():
    """Отдает главную страницу"""
    data_common = {}
    data = {'common': data_common}
    return render_template('index.html',
                           title='Олимп',
                           data=data)


def to_table(form):
    """Преобразует форму в табличку"""
    template = '''
    <table class="table table-sm table-borderless">
        <colgroup>
           <col span="1" style="width: 10%;">
           <col span="1" style="width: 20%;">
           <col span="1" style="width: 50%;">
           <col span="1" style="width: 10%;">
           <col span="1" style="width: 10%;">
        </colgroup>
        <thead> 
            <tr> 
                <th>#</th> 
                <th>ID</th> 
                <th>Код</th> 
                <th>Наименование</th> 
                <th>Кол-во БД</th> 
                <th>Кол-во факт</th> 
            </tr> 
        </thead> 
        <tbody>
            {{ form.csrf_token }}
            {% for item in form.items %}
                <tr>
                    {% for field in item %}
                        <td>{{ field() }}</td>
                    {% endfor %}
                </tr>
            {% endfor %}
        </tbody>
    </table>'''
    return Template(template, autoescape=True).render(form=form)


class InventoryItemForm(FlaskForm):
    row_number = IntegerField('#', render_kw={'class': 'border-0', 'readonly': ''})
    good_id = IntegerField('ID', render_kw={'class': 'border-0', 'readonly': ''})
    good_code = StringField('Код', render_kw={'class': 'border-0', 'readonly': ''})
    good_name = StringField('Наименование', render_kw={'class': 'border-0', 'readonly': ''})
    quantity_db = IntegerField('Кол-во БД', render_kw={'class': 'border-0', 'readonly': ''})
    quantity_fact = IntegerField('Кол-во факт', validators=[NumberRange(min=0, message='Укажите положительное число')])


class InventoryForm(FlaskForm):
    items = FieldList(FormField(InventoryItemForm))


@app.route('/inventory', methods=['GET', 'POST'])
def update_inventory():
    """Выводит остатки и записывает изменения в БД"""
    inventory_form = InventoryForm(request.form)
    if request.method == 'POST' and inventory_form.validate():
        # получаем данные инвентаризации и записываем их в БД
        db.set_goods_balance(inventory_form.data)
        return jsonify(status='OK')  # возвращаем статус в виде JSON
    else:
        for item in db.get_goods_balance():  # получаем остатки товаров и заполняем форму перед выводом
            item_form = InventoryItemForm()
            item_form.row_number = item['row_number']
            item_form.good_id = item['good_id']
            item_form.good_code = item['good_code']
            item_form.good_name = item['good_name']
            item_form.quantity_db = item['quantity_db']
            item_form.quantity_fact = item['quantity_fact']
            inventory_form.items.append_entry(item_form)  # добавляем форму в набор
        return jsonify(inventory=to_table(inventory_form))  # отправляем данные в виде JSON
