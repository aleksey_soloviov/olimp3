### Зависимости
Python 3.7

### Установка библиотек
python -m pip install -r requirements.txt

### Запуск
python run.py

### Полезные ссылки
[Flask](https://flask.palletsprojects.com/en/1.1.x/)

[Bootstrap 4](https://getbootstrap.com/docs/4.4/getting-started/introduction/)

[JQuery](https://api.jquery.com)
